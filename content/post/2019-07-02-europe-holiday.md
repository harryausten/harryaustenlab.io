---
title: "Europe Holiday"
date: 2019-07-02
bigimg: [{src: "/img/2019-07-02-europe-holiday/49.jpg", desc: "VW Caddy Camper"}]
---

My girlfriend, Ellen, and I recently got back from a 3 week holiday travelling round Europe in a campervan. Here are some photos taken during the trip...

<!--more-->

{{< gallery dir="/img/2019-07-02-europe-holiday/" caption-position="none" hover-effect="grow" hover-transition="none" />}}
{{< load-photoswipe >}}
