---
title: First Post!
date: 2019-05-25
---

I have now setup my own personal website using [GitLab Pages](https://about.gitlab.com/product/pages/). Check out the source code [here](https://gitlab.com/harryausten/harryausten.gitlab.io).
