---
title: About me
---

My name is Harry Austen. I am a 21 year old who has just finished a degree in Mathematics at the University of Bath.

Here are a few of my interests:

- All things Linux
- Computer hardware and construction
- Programming
- I play the cornet in a brass band
