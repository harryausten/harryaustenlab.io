![Build Status](https://gitlab.com/harryausten/harryausten.gitlab.io/badges/master/build.svg)

Personal website built with [Hugo][] using [GitLab Pages][pages].

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Hugo
1. Preview your project: `hugo server`
1. Add content
1. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation][].

### Preview the site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/`.

The theme used is adapted from http://themes.gohugo.io/beautifulhugo/.

[ci]: https://about.gitlab.com/gitlab-ci/
[documentation]: https://gohugo.io/overview/introduction/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[pages]: https://pages.gitlab.io
